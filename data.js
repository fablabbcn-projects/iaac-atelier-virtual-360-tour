var APP_DATA = {
  "scenes": [
    {
      "id": "0-entrance-p59",
      "name": "Entrance P59",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.5665114044013961,
        "pitch": 0.02658707690431683,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 0.23579391175742792,
          "pitch": -0.031421358285646406,
          "rotation": 0,
          "target": "1-main-entrance"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-main-entrance",
      "name": "Main Entrance",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.44325208232633173,
        "pitch": 0.3656893065359377,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -2.914924013311886,
          "pitch": 0.04232443670662356,
          "rotation": 4.71238898038469,
          "target": "10-atelier1"
        },
        {
          "yaw": -1.9464160827293266,
          "pitch": 0.03785592355004752,
          "rotation": 7.853981633974483,
          "target": "2-main-1"
        },
        {
          "yaw": 1.430439725700821,
          "pitch": 0.08922567724806285,
          "rotation": 0,
          "target": "0-entrance-p59"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.9372317757833244,
          "pitch": 0.0535820501051365,
          "title": "Bicycle Rack",
          "text": ""
        },
        {
          "yaw": -0.6112015722781869,
          "pitch": -0.10435570538657046,
          "title": "Electronics Room",
          "text": ""
        }
      ]
    },
    {
      "id": "2-main-1",
      "name": "Main-1",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.15565571748589235,
        "pitch": -0.11942201750805026,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 0.6604343607587495,
          "pitch": 0.03461766789853549,
          "rotation": 1.5707963267948966,
          "target": "1-main-entrance"
        },
        {
          "yaw": 1.0351993476355048,
          "pitch": 0.026174895501421602,
          "rotation": 0,
          "target": "0-entrance-p59"
        },
        {
          "yaw": 2.520236752574144,
          "pitch": 0.06978490085552558,
          "rotation": 10.995574287564278,
          "target": "10-atelier1"
        },
        {
          "yaw": -0.17287472521283753,
          "pitch": 0.23926115540444926,
          "rotation": 0,
          "target": "3-main-2"
        },
        {
          "yaw": -0.5334155547830797,
          "pitch": 0.03139195179927334,
          "rotation": 4.71238898038469,
          "target": "9-lasercutters"
        },
        {
          "yaw": -0.7425692658797782,
          "pitch": -0.9510457449120846,
          "rotation": 0.7853981633974483,
          "target": "10-atelier1"
        },
        {
          "yaw": -2.201903200889502,
          "pitch": -0.8916525690849575,
          "rotation": 5.497787143782138,
          "target": "11-vr-room"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 3.105019138010652,
          "pitch": 0.35081851902441485,
          "title": "Drone Cage",
          "text": "Aerial Testing area"
        },
        {
          "yaw": -0.9422370329195218,
          "pitch": 0.022173487139411918,
          "title": "Material Storage",
          "text": ""
        },
        {
          "yaw": -1.5870661828232517,
          "pitch": 0.042453632738620684,
          "title": "Lift",
          "text": ""
        }
      ]
    },
    {
      "id": "3-main-2",
      "name": "Main-2",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.426375656580106,
        "pitch": -0.04072381286968074,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 0.2119591294525751,
          "pitch": 0.04461567133246014,
          "rotation": 0,
          "target": "4-main-3"
        },
        {
          "yaw": -0.402959950818202,
          "pitch": -0.10824486640791164,
          "rotation": 3.141592653589793,
          "target": "5-robot-arms"
        },
        {
          "yaw": -1.24209892811961,
          "pitch": -0.10058409166149396,
          "rotation": 3.141592653589793,
          "target": "9-lasercutters"
        },
        {
          "yaw": -2.951995525294821,
          "pitch": 0.2441668239628907,
          "rotation": 0,
          "target": "2-main-1"
        },
        {
          "yaw": 3.1031167574447878,
          "pitch": 0.0023230237938616227,
          "rotation": 4.71238898038469,
          "target": "10-atelier1"
        },
        {
          "yaw": 2.7424892523773963,
          "pitch": 0.07501423343894587,
          "rotation": 10.995574287564278,
          "target": "1-main-entrance"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 1.606692073168574,
          "pitch": 0.011279695516087429,
          "title": "3DPrinting-Electronics Room",
          "text": ""
        },
        {
          "yaw": 0.6933308631004156,
          "pitch": 0.035647458566430146,
          "title": "Staff Office",
          "text": ""
        }
      ]
    },
    {
      "id": "4-main-3",
      "name": "Main-3",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.1146346595973,
        "pitch": 0.05964212876762076,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 1.847239872988399,
          "pitch": 0.09816184959605856,
          "rotation": 10.995574287564278,
          "target": "6-workshop"
        },
        {
          "yaw": -2.493874993732769,
          "pitch": 0.10769341756367545,
          "rotation": 5.497787143782138,
          "target": "13-atelier3"
        },
        {
          "yaw": -0.13765944901779292,
          "pitch": 0.01465626037742318,
          "rotation": 0,
          "target": "3-main-2"
        },
        {
          "yaw": 0.38748864640430725,
          "pitch": 0.019676419640624943,
          "rotation": 1.5707963267948966,
          "target": "8-cnc-workshop"
        },
        {
          "yaw": 0.13686573198885377,
          "pitch": 0.015607665892273914,
          "rotation": 1.5707963267948966,
          "target": "5-robot-arms"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.1897785094077449,
          "pitch": 0.029188086331291885,
          "title": "Classroom",
          "text": ""
        },
        {
          "yaw": 2.7541339739756348,
          "pitch": 0.06667653276472763,
          "title": "Matter Lab",
          "text": "Safety first"
        },
        {
          "yaw": -2.0441429029656497,
          "pitch": 0.05499287666211039,
          "title": "Research projects",
          "text": "Storage area"
        },
        {
          "yaw": 1.022284427751087,
          "pitch": 0.07417559613717017,
          "title": "Tools Storage",
          "text": ""
        }
      ]
    },
    {
      "id": "5-robot-arms",
      "name": "Robot Arms",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.240293815795944,
        "pitch": 0.15018102741121453,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -2.4850575435545004,
          "pitch": -0.0085972804665424,
          "rotation": 3.141592653589793,
          "target": "9-lasercutters"
        },
        {
          "yaw": 2.533253024214673,
          "pitch": 0.013462831451333201,
          "rotation": 3.141592653589793,
          "target": "3-main-2"
        },
        {
          "yaw": 3.0921242754921154,
          "pitch": 0.01551633851872225,
          "rotation": 6.283185307179586,
          "target": "2-main-1"
        },
        {
          "yaw": 1.4804775840356692,
          "pitch": 0.0551348466894801,
          "rotation": 4.71238898038469,
          "target": "4-main-3"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.8956830390724235,
          "pitch": -0.09318913639896742,
          "title": "ABB IRB120",
          "text": ""
        },
        {
          "yaw": -0.6820573434535859,
          "pitch": -0.2858185863842735,
          "title": "ABB IRB140",
          "text": ""
        },
        {
          "yaw": 2.800166592541622,
          "pitch": 0.20659720814207283,
          "title": "3D Scanner",
          "text": ""
        }
      ]
    },
    {
      "id": "6-workshop",
      "name": "Workshop",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.5514130100495187,
        "pitch": 0.007215767981904264,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 3.0083386033547663,
          "pitch": -0.04515470808669164,
          "rotation": 3.141592653589793,
          "target": "7-wood-workshop"
        },
        {
          "yaw": 0.028378218687070245,
          "pitch": 0.03807945786883238,
          "rotation": 3.141592653589793,
          "target": "8-cnc-workshop"
        },
        {
          "yaw": -1.3173094893069148,
          "pitch": 0.04749675487847327,
          "rotation": 1.5707963267948966,
          "target": "4-main-3"
        },
        {
          "yaw": -1.6395675204857874,
          "pitch": 0.04852043117207927,
          "rotation": 4.71238898038469,
          "target": "13-atelier3"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.1836265159078643,
          "pitch": -0.03953855540038731,
          "title": "Hand Tools",
          "text": ""

        },
        {
          "yaw": 0.673580043899495,
          "pitch": 0.24319109974062592,
          "title": "CNC",
          "text": "Shopbot 120-60"
        },
        {
          "yaw": -0.36039447438498584,
          "pitch": 0.17590813551490037,
          "title": "CNC",
          "text": "T-REX S1215"
        }
      ]
    },
    {
      "id": "7-wood-workshop",
      "name": "Wood Workshop",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.11960385251106054,
        "pitch": 0.016437305977346384,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -0.3815440059226489,
          "pitch": 0.0988567959492137,
          "rotation": 3.141592653589793,
          "target": "6-workshop"
        },
        {
          "yaw": -0.38549710192928544,
          "pitch": -0.08413710411253561,
          "rotation": 0,
          "target": "8-cnc-workshop"
        },
        {
          "yaw": -0.7095534778010855,
          "pitch": 0.00440011886600189,
          "rotation": 4.71238898038469,
          "target": "4-main-3"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "8-cnc-workshop",
      "name": "Cnc Workshop",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -1.3116572024136453,
        "pitch": -0.029765990432981226,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -1.4822526601322519,
          "pitch": 0.023077232766116396,
          "rotation": 3.141592653589793,
          "target": "6-workshop"
        },
        {
          "yaw": -0.9535436854828809,
          "pitch": 0.03364683479789221,
          "rotation": 1.5707963267948966,
          "target": "4-main-3"
        },
        {
          "yaw": 0.5716188710132322,
          "pitch": 0.05874490062481286,
          "rotation": 1.5707963267948966,
          "target": "3-main-2"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.4238716068180661,
          "pitch": 0.4592364471696868,
          "title": "CNC",
          "text": "T-REX S1215"
        },
        {
          "yaw": -2.268458344298775,
          "pitch": 0.31916541643510143,
          "title": "CNC",
          "text": "Shopbot 120-60"
        }
      ]
    },
    {
      "id": "9-lasercutters",
      "name": "Lasercutters",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.4692035871157554,
        "pitch": 0.1273809600027498,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 2.288519878223461,
          "pitch": -0.04827148262052461,
          "rotation": 3.141592653589793,
          "target": "5-robot-arms"
        },
        {
          "yaw": -2.316389875739958,
          "pitch": 0.040488006138200916,
          "rotation": 3.141592653589793,
          "target": "3-main-2"
        },
        {
          "yaw": -1.7146031563190913,
          "pitch": 0.03170869958731615,
          "rotation": 1.5707963267948966,
          "target": "2-main-1"
        },
        {
          "yaw": -3.1283498767572517,
          "pitch": 0.014675567160324476,
          "rotation": 4.71238898038469,
          "target": "4-main-3"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 1.3681909953534852,
          "pitch": 0.23309467020882302,
          "title": "Lasercutters",
          "text": "EpilogLegend 36EXT"
        },
        {
          "yaw": -0.5445707646779798,
          "pitch": 0.25017036061659503,
          "title": "Big Lasercutter",
          "text": "C02 -120watts"
        }
      ]
    },
    {
      "id": "10-atelier1",
      "name": "Atelier1",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.5218722596733478,
        "pitch": -0.05313564247468605,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 0.14364480991129014,
          "pitch": 0.022606888166803785,
          "rotation": 1.5707963267948966,
          "target": "11-vr-room"
        },
        {
          "yaw": -0.7758738706996855,
          "pitch": 0.0002570676234725511,
          "rotation": 0,
          "target": "2-main-1"
        },
        {
          "yaw": -2.135836016676169,
          "pitch": 0.03448018486088955,
          "rotation": 4.71238898038469,
          "target": "12-atelier2"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.2404263201190329,
          "pitch": 0.24867638927586277,
          "title": "Students Workspace",
          "text": "IaaC Life"
        }
      ]
    },
    {
      "id": "11-vr-room",
      "name": "Vr Room",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -1.052319035492724,
        "pitch": -0.051892827279219844,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -0.3153117098162319,
          "pitch": 0.01533873395415597,
          "rotation": 3.141592653589793,
          "target": "10-atelier1"
        },
        {
          "yaw": 0.4709295127093558,
          "pitch": -0.010473993219406452,
          "rotation": 4.71238898038469,
          "target": "12-atelier2"
        },
        {
          "yaw": 1.0325395315828292,
          "pitch": 0.0029084089647497535,
          "rotation": 1.5707963267948966,
          "target": "2-main-1"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "12-atelier2",
      "name": "Atelier2",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.02039178923159568,
        "pitch": -0.02728784491150371,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -0.524527347118358,
          "pitch": 0.026942904056685535,
          "rotation": 4.71238898038469,
          "target": "13-atelier3"
        },
        {
          "yaw": 0.729632183675049,
          "pitch": 0.032780091388600496,
          "rotation": 7.853981633974483,
          "target": "10-atelier1"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "13-atelier3",
      "name": "Atelier3",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.7527058968334224,
        "pitch": -0.027717068193204852,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 0.3369141469940651,
          "pitch": 0.027681762075015826,
          "rotation": 1.5707963267948966,
          "target": "12-atelier2"
        },
        {
          "yaw": 0.14961398262525805,
          "pitch": 0.02073719091494297,
          "rotation": 0,
          "target": "10-atelier1"
        },
        {
          "yaw": 1.9179992526575713,
          "pitch": 0.050174969489443555,
          "rotation": 7.853981633974483,
          "target": "14-conference-room"
        },
        {
          "yaw": -2.3893211192030765,
          "pitch": 0.2575959258225051,
          "rotation": 5.497787143782138,
          "target": "4-main-3"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.24989926371899074,
          "pitch": 0.31520245987641893,
          "title": "Students Workspace",
          "text": "IaaC Life"
        }
      ]
    },
    {
      "id": "14-conference-room",
      "name": "Conference Room",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.20867454936495733,
        "pitch": 0.0433300117306068,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 0.42100569727675463,
          "pitch": 0.04725016210576527,
          "rotation": 7.0685834705770345,
          "target": "13-atelier3"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "Iaac Atelier",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": true,
    "viewControlButtons": true
  }
};
