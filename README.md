# IaaC Atelier Pujades,59 - Virtual 360 Tour

[Link to the tour](https://fablabbcn-projects.gitlab.io/iaac-atelier-virtual-360-tour/)

![](p59.jpg)

### Contact
Made in 20/05/2020 by:

- **eduardo.chamorro@iaac.net**
- **josep@fablabbcn.org**

Thanks

### License

GNU General Public License v3.0 for software (original software credited below). (See LICENSE file).

Cern OHL V1.2 for the Machine Files. (See CERN_OHL file)

### Credits

Made with [Marzipano](https://www.marzipano.net/) and a Insta 360 One X.

##### Disclaimer

<div class="align-justify">
This hardware/software is provided "as is", and you use the hardware/software at your own risk. Under no circumstances shall any author be liable for direct, indirect, special, incidental, or consequential damages resulting from the use, misuse, or inability to use this hardware/software, even if the authors have been advised of the possibility of such damages.</div>
